using System;
using Shouldly;
using Xunit;
using Zooland.Core.Entities;
using Zooland.Core.Exceptions;

namespace Zooland.Tests.Domain
{
    public class AnimalTests
    {
        [Theory]
        [InlineData(" ")]
        [InlineData("    \t    ")]
        [InlineData(null)]
        [InlineData("\t")]
        public void ChangeName_Should_Throw_Exception_When_Given_Name_Is_Invalid(string name)
        {
            var animal = new Animal(Guid.Empty, "name", "category", Core.Enums.AnimalClass.Fish, "photoUrl");

            var exception = Record.Exception(() => animal.ChangeName(name));

            exception.ShouldNotBeNull();
            exception.ShouldBeAssignableTo<DomainException>();
        }

        [Theory]
        [InlineData(" ")]
        [InlineData("    \t    ")]
        [InlineData(null)]
        [InlineData("\t")]
        public void ChangePhoto_Should_Throw_Exception_When_Given_PhotoUrl_Is_Invalid(string photoUrl)
        {
            var animal = new Animal(Guid.Empty, "name", "category", Core.Enums.AnimalClass.Fish, "photoUrl");

            var exception = Record.Exception(() => animal.ChangePhoto(photoUrl));

            exception.ShouldNotBeNull();
            exception.ShouldBeAssignableTo<DomainException>();
        }

        [Fact]
        public void MoveToOtherPlace_Should_Update_Property_PlaceInZoo_When_Given_Place_Is_Valid()
        {
            var animal = new Animal(Guid.Empty, "name", "category", Core.Enums.AnimalClass.Fish, "photoUrl");
            var place = new Place(Guid.Empty, "TestPlace");

            animal.MoveToOtherPlace(place);

            animal.PlaceInZoo.ShouldNotBeNull();
            animal.PlaceInZoo.Name.ShouldBe("TestPlace");
        }

        [Fact]
        public void MoveToOtherPlace_Should_Throw_Exception_When_Given_Place_Is_Null()
        {
            var animal = new Animal(Guid.Empty, "name", "category", Core.Enums.AnimalClass.Fish, "photoUrl");

            var exception = Record.Exception(() => animal.MoveToOtherPlace(null));

            exception.ShouldNotBeNull();
            exception.ShouldBeAssignableTo<DomainException>();
        }
    }
}