using System;
using System.Threading.Tasks;
using NSubstitute;
using Shouldly;
using Xunit;
using Zooland.Application.Commands;
using Zooland.Application.Commands.Animals;
using Zooland.Application.Commands.Animals.Handlers;
using Zooland.Application.Exceptions.Animal;
using Zooland.Application.Services;
using Zooland.Core.Entities;
using Zooland.Core.Repositories;

namespace Zooland.Tests.Handlers.Animals
{
public class CreateAnimalHandlerTests
    {
        Task Act(CreateAnimal command)
            => _handler.HandleAsync(command);

        [Fact]
        public async Task HandleAsync_Should_Throw_An_Exception_When_Animal_With_Given_Id_Already_Exists()
        {
            var command = new CreateAnimal(Guid.NewGuid(), "Name", "Category", Core.Enums.AnimalClass.Amphibian, "PhotoUrl");
            var animal = new Animal(command.Id, "Name", "Category", Core.Enums.AnimalClass.Amphibian, "PhotoUrl");
            _repository.GetAsync(command.Id).Returns(animal);

            var exception = await Record.ExceptionAsync(async () => await Act(command));

            exception.ShouldNotBeNull();
            exception.ShouldBeAssignableTo<AnimalAlreadyExistsException>();
        }

        [Fact]
        public async Task HandleAsync_Should_Create_Animal_With_Given_Data_Using_Repository()
        {
            var command = new CreateAnimal(Guid.NewGuid(), "Name", "Category", Core.Enums.AnimalClass.Amphibian, "PhotoUrl");

            await Act(command);

            await _repository
                .Received(1)
                .AddAsync(Arg.Is<Animal>(d =>
                    d.Id == command.Id &&
                    d.Name == command.Name &&
                    d.Category == command.Category &&
                    d.Classification == command.Classification &&
                    d.PhotoUrl == command.PhotoUrl));
        }

        #region ARRANGE
        private readonly ICommandHandler<CreateAnimal> _handler;
        private readonly IAnimalRepository _repository;
        private readonly IMessageBroker _broker;

        public CreateAnimalHandlerTests()
        {
            _repository = Substitute.For<IAnimalRepository>();
            _broker = Substitute.For<IMessageBroker>();
            _handler = new CreateAnimalHandler(_repository, _broker);
        }
        #endregion
    }
}