using System;
using System.Threading.Tasks;
using NSubstitute;
using Shouldly;
using Xunit;
using Zooland.Application.Commands;
using Zooland.Application.Commands.Animals;
using Zooland.Application.Commands.Animals.Handlers;
using Zooland.Application.Exceptions.Animal;
using Zooland.Application.Services;
using Zooland.Core.Entities;
using Zooland.Core.Repositories;

namespace Zooland.Tests.Handlers.Animals
{
    public class UpdateAnimalHandlerTests
    {
        Task Act(UpdateAnimal command)
            => _handler.HandleAsync(command);

        [Fact]
        public async Task HandleAsync_Should_Throw_An_Exception_When_Animal_With_Given_Id_Does_Not_Exist()
        {

            var command = new UpdateAnimal(Guid.NewGuid(), "New Name", "No Avatar");

            var exception = await Record.ExceptionAsync(async () => await Act(command));

            exception.ShouldNotBeNull();
            exception.ShouldBeAssignableTo<AnimalDoesNotExistException>();
        }

        [Fact]
        public async Task HandleAsync_Should_Update_Animal_With_Given_Id_Using_Repository()
        {
            var command = new UpdateAnimal(Guid.NewGuid(), "New Name", "NoPhoto");
            var animal = new Animal(command.Id, "Name", "Category", Core.Enums.AnimalClass.Amphibian, "PhotoUrl");
            _repository.GetAsync(command.Id).Returns(animal);

            await Act(command);

            await _repository.Received(1).UpdateAsync(animal);
        }

        #region ARRANGE
        private readonly ICommandHandler<UpdateAnimal> _handler;
        private readonly IAnimalRepository _repository;
        private readonly IMessageBroker _broker;

        public UpdateAnimalHandlerTests()
        {
            _repository = Substitute.For<IAnimalRepository>();
            _broker = Substitute.For<IMessageBroker>();
            _handler = new UpdateAnimalHandler(_repository,_broker);
        }
        #endregion

    }
}