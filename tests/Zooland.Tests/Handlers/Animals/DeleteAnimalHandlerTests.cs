using System;
using System.Threading.Tasks;
using NSubstitute;
using Shouldly;
using Xunit;
using Zooland.Application.Commands;
using Zooland.Application.Commands.Animals;
using Zooland.Application.Commands.Animals.Handlers;
using Zooland.Application.Exceptions.Animal;
using Zooland.Application.Services;
using Zooland.Core.Entities;
using Zooland.Core.Repositories;

namespace Zooland.Tests.Handlers.Animals
{
    public class DeleteAnimalHandlerTests
    {

        Task Act(DeleteAnimal command)
            => _handler.HandleAsync(command);

        [Fact]
        public async Task HandleAsync_Should_Throw_An_Exception_When_Animal_With_Given_Id_Does_Not_Exist()
        {
            var command = new DeleteAnimal(Guid.NewGuid());

            var exception = await Record.ExceptionAsync(async () => await Act(command));

            exception.ShouldNotBeNull();
            exception.ShouldBeAssignableTo<AnimalDoesNotExistException>();
        }
        [Fact]
        public async Task HandleAsync_Should_Delete_Animal_With_Given_Id_Using_Repository()
        {
            var command = new DeleteAnimal(Guid.NewGuid());
            var animal = new Animal(command.Id, "Name", "Category", Core.Enums.AnimalClass.Amphibian, "PhotoUrl");

            _repository.GetAsync(command.Id).Returns(animal);

            await Act(command);
            await _repository.Received(1).DeleteAsync(animal);
        }

        #region ARRANGE
        private readonly ICommandHandler<DeleteAnimal> _handler;
        private readonly IAnimalRepository _repository;
        private readonly IMessageBroker _broker;

        public DeleteAnimalHandlerTests()
        {
            _repository = Substitute.For<IAnimalRepository>();
            _broker = Substitute.For<IMessageBroker>();
            _handler = new DeleteAnimalHandler(_repository, _broker);
        }
        #endregion
    }
}