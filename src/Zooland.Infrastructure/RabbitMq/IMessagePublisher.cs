using System.Threading.Tasks;

namespace Zooland.Infrastructure.RabbitMq
{
    public interface IMessagePublisher
    {
        Task PublishAsync<TMessage>(TMessage message) where TMessage : class;
    }
}