using Zooland.Application.Commands;
using Zooland.Application.Commands.Dispatchers;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Zooland.Application.Events;
using Zooland.Application.Events.Dispatchers;

namespace Zooland.Infrastructure.RabbitMq.CQRS
{
    internal static class Extensions
    {
        public static void SubscribeCommand<TCommand>(this IMessageSubscriber subscriber)
            where TCommand : class, ICommand
            => subscriber.Subscribe<TCommand>(async (sp, command) =>
            {
                var dispatcher = sp.GetService<ICommandDispatcher>();
                await dispatcher.DispatchAsync(command);
            });

        public static void SubscribeEvent<TEvent>(this IMessageSubscriber subscriber)
            where TEvent : class, IEvent
            => subscriber.Subscribe<TEvent>(async (sp, @event) =>
            {
                var dispatcher = sp.GetService<IEventDispatcher>();
                await dispatcher.DispatchAsync(@event);
            });

        public static Task PublishCommandAsync<TCommand>(this IMessagePublisher publisher,
            TCommand command)
            where TCommand : class, ICommand
            => publisher.PublishAsync(command);

        public static Task PublishEventAsync<TEvent>(this IMessagePublisher publisher,
            TEvent @event)
            where TEvent : class, IEvent
            => publisher.PublishAsync(@event);
    }
}