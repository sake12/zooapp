using System;
using System.Threading.Tasks;

namespace Zooland.Infrastructure.RabbitMq
{
    public interface IMessageSubscriber
    {
        void Subscribe<TMessage>(Func<IServiceProvider, TMessage, Task> onReceived) where TMessage : class;
    }
}