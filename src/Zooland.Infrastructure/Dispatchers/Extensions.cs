using Microsoft.Extensions.DependencyInjection;
using Zooland.Application.Commands.Dispatchers;
using Zooland.Application.Events.Dispatchers;
using Zooland.Application.Queries.Dispatchers;

namespace Zooland.Infrastructure.Dispatchers
{
    internal static class Extensions
    {
        public static void AddDispatchers(this IServiceCollection services)
        {
            services.AddTransient<ICommandDispatcher, CommandDispatcher>();
            services.AddTransient<IEventDispatcher, EventDispatcher>();
            services.AddTransient<IQueryDispatcher, QueryDispatcher>();
        }
    }
}