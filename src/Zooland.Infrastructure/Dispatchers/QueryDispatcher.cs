using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Zooland.Application.Queries;
using Zooland.Application.Queries.Dispatchers;
using Zooland.Application.Queries.Handlers;

namespace Zooland.Infrastructure.Dispatchers
{
    public class QueryDispatcher : IQueryDispatcher
    {
        private readonly IServiceProvider _serviceProdiver;

        public QueryDispatcher(IServiceProvider serviceProdiver)
            => _serviceProdiver = serviceProdiver;

        public Task<TResult> DispatchAsync<TQuery, TResult>(TQuery query) where TQuery : IQuery<TResult>
        {
            var handler = _serviceProdiver.GetService<IQueryHandler<TQuery, TResult>>();
            return handler.HandleAsync(query);
        }
    }
}