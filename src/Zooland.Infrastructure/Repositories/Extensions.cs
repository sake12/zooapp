using Microsoft.Extensions.DependencyInjection;
using Zooland.Core.Repositories;

namespace Zooland.Infrastructure.Repositories
{
    internal static class Extensions
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IAnimalRepository, AnimalsDatabaseRepository>();
        }
    }
}