using System;
using System.Threading.Tasks;
using Zooland.Core.Entities;
using Zooland.Core.Repositories;
using Zooland.Infrastructure.Database;
using Zooland.Infrastructure.Database.nHibernate.Entities;
using Zooland.Infrastructure.Database.nHibernate.Entities.Extensions;

namespace Zooland.Infrastructure.Repositories
{
    internal class AnimalsDatabaseRepository : IAnimalRepository
    {
        private readonly IRepository<AnimalEntity> _repository;

        public AnimalsDatabaseRepository(IRepository<AnimalEntity> repository)
            => _repository = repository;

        public async Task<Animal> GetAsync(Guid id)
        {
            var entity = await _repository.GetAsync(id);
            return entity?.AsAnimal();
        }

        public async Task AddAsync(Animal animal)
            => await _repository.AddAsync(animal.AsEntity());

        public async Task UpdateAsync(Animal animal)
            => await _repository.UpdateAsync(animal.AsEntity());

        public async Task DeleteAsync(Animal animal)
            => await _repository.DeleteAsync(animal.AsEntity());
    }
}