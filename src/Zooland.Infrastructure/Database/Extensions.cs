using Microsoft.Extensions.DependencyInjection;
using Zooland.Infrastructure.Database.nHibernate;
using Zooland.Infrastructure.Options;

namespace Zooland.Infrastructure.Database
{
    internal static class Extensions
    {
        public static void AddDatabase(this IServiceCollection services)
        {
            services.AddOption<DatabaseOptions>("Database");
            services.AddNHibernate();
        }
    }
}