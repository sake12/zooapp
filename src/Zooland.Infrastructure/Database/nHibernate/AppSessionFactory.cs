using Zooland.Infrastructure.Database.nHibernate.Entities.Mappings;
using Zooland.Infrastructure.Options;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;

namespace Zooland.Infrastructure.Database.nHibernate
{
    internal sealed class AppSessionFactory
    {
        private readonly ISessionFactory _sessionFactory;

        public AppSessionFactory(DatabaseOptions options)
        {
            var mapper = new ModelMapper();
            mapper.AddMapping<AnimalEntityMapping>();
            var domainMapping = mapper.CompileMappingForAllExplicitlyAddedEntities();

            var configuration = new Configuration();
            configuration.SessionFactoryName(options.InstanceName);
            configuration.DataBaseIntegration(db =>
                {
                    db.ConnectionString = options.ConnectionString;
                    db.Dialect<MsSql2012Dialect>();
                    db.Driver<SqlClientDriver>();
                })
                .AddMapping(domainMapping);
            _sessionFactory = configuration.BuildSessionFactory();

            new SchemaUpdate(configuration).Execute(false, true);
        }

        public ISession OpenSession()
            => _sessionFactory.OpenSession();
    }
}