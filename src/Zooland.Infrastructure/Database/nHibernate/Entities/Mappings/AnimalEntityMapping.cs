using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Zooland.Core.Enums;

namespace Zooland.Infrastructure.Database.nHibernate.Entities.Mappings
{
    internal class AnimalEntityMapping : ClassMapping<AnimalEntity>
    {
        public AnimalEntityMapping()
        {
            Id(x => x.Id);
            Property(x => x.Name, map => map.NotNullable(true));
            Property(x => x.PhotoUrl, map => map.NotNullable(true));
            Property(x => x.Category, map => map.NotNullable(true));

            Property(x => x.Classification, map =>
            {
                map.Type<EnumStringType<AnimalClass>>();
                map.NotNullable(true);
            });

            Property(x => x.IsDeleted, map =>
            {
                map.NotNullable(true);
                map.Type<YesNoType>();
            });
        }
    }
}