using Zooland.Core.Enums;
using Zooland.Infrastructure.Database.Models;

namespace Zooland.Infrastructure.Database.nHibernate.Entities
{
    internal class AnimalEntity : SoftDeletableDbModel
    {
        public virtual string Name { get; set; }
        public virtual AnimalClass Classification { get; set; }
        public virtual string Category { get; set; }
        public virtual string PhotoUrl { get; set; }
    }
}