using System.Collections.Generic;
using System.Linq;
using Zooland.Application.DTO;
using Zooland.Core.Entities;

namespace Zooland.Infrastructure.Database.nHibernate.Entities.Extensions
{
    internal static class AnimalExtensions
    {
        public static AnimalEntity AsEntity(this Animal animal)
            => new AnimalEntity
            {
                Id = animal.Id,
                Name = animal.Name,
                Category = animal.Category,
                Classification = animal.Classification,
                PhotoUrl = animal.PhotoUrl
            };

        public static Animal AsAnimal(this AnimalEntity entity)
            => new Animal(entity.Id, entity.Name, entity.Category, entity.Classification, entity.PhotoUrl);

        public static IEnumerable<AnimalDto> AsDtos(this IEnumerable<AnimalEntity> animals)
            => animals.Select(a => new AnimalDto
            {
                Id = a.Id,
                Name = a.Name,
                Category = a.Category,
                Classification = a.Classification,
                PhotoUrl = a.PhotoUrl
            });

        public static AnimalDto AsDto(this AnimalEntity animal)
            => new AnimalDto
            {
                Id = animal.Id,
                Name = animal.Name,
                Category = animal.Category,
                Classification = animal.Classification,
                PhotoUrl = animal.PhotoUrl
            };
    }
}