namespace Zooland.Infrastructure.Database.Models
{
    internal abstract class SoftDeletableDbModel : DbModel, ISoftDeletable
    {
        void ISoftDeletable.SoftDelete()
            => IsDeleted = true;
    }
}