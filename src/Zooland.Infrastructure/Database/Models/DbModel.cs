using System;

namespace Zooland.Infrastructure.Database.Models
{
    internal abstract class DbModel
    {
        public virtual Guid Id { get; set; }
        public virtual bool IsDeleted { get; protected set; }
    }
}