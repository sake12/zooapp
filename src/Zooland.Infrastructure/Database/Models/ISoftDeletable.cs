namespace Zooland.Infrastructure.Database.Models
{
    internal interface ISoftDeletable
    {
        bool IsDeleted { get; }
        void SoftDelete();
    }
}