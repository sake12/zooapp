using Microsoft.Extensions.DependencyInjection;
using Zooland.Infrastructure.Dispatchers;
using Microsoft.AspNetCore.Builder;
using Zooland.Infrastructure.Database;
using Zooland.Infrastructure.Repositories;
using Zooland.Infrastructure.Queries.Handlers;
using Zooland.Infrastructure.RabbitMq;
using Zooland.Infrastructure.RabbitMq.CQRS;
using Zooland.Application.Services;
using Zooland.Infrastructure.Services;
using Zooland.Application.Events.Animal;
using Zooland.Infrastructure.Swagger;

namespace Zooland.Infrastructure
{
    public static class Extensions
    {
        public static void RegisterInfrastructure(this IServiceCollection services)
        {
            services.AddRabbitMq();
            services.AddSwagger();

            services.AddDatabase();
            services.AddRepositories();

            services.AddQueryHandlers();
            services.AddDispatchers();

            services.AddTransient<IMessageBroker, MessageBroker>();
        }

        public static void UseInfrastructure(this IApplicationBuilder app)
        {
            app.UseSwaggerExtension();
            var subscriber = app.ApplicationServices.GetService<IMessageSubscriber>();

            subscriber.SubscribeEvent<AnimalCreated>();
            subscriber.SubscribeEvent<AnimalDeleted>();
            subscriber.SubscribeEvent<AnimalUpdated>();
        }
    }
}