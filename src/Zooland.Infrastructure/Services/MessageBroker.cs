using System.Linq;
using System.Threading.Tasks;
using Zooland.Application.Events;
using Zooland.Application.Services;
using Zooland.Infrastructure.RabbitMq;
using Zooland.Infrastructure.RabbitMq.CQRS;

namespace Zooland.Infrastructure.Services
{
    internal sealed class MessageBroker : IMessageBroker
    {
        private readonly IMessagePublisher _publisher;

        public MessageBroker(IMessagePublisher publisher)
            => _publisher = publisher;

        public async Task PublishAsync(params IEvent[] events)
        {
            var tasks = events.Select(e => _publisher.PublishEventAsync(e));
            await Task.WhenAll(tasks);
        }
    }
}