using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Zooland.Application.DTO;
using Zooland.Application.Queries.Animal;
using Zooland.Application.Queries.Handlers;
using Zooland.Infrastructure.Queries.Handlers.Animal;

namespace Zooland.Infrastructure.Queries.Handlers
{
    internal static class Extensions
    {
        public static void AddQueryHandlers(this IServiceCollection services)
        {
            services.AddTransient<IQueryHandler<GetAnimal, AnimalDto>, GetAnimalHandler>();
            services.AddTransient<IQueryHandler<SearchAnimals, IEnumerable<AnimalDto>>, SearchAnimalsHandler>();
        }
    }
}