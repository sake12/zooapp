using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zooland.Application.DTO;
using Zooland.Application.Queries.Animal;
using Zooland.Application.Queries.Handlers;
using Zooland.Infrastructure.Database;
using Zooland.Infrastructure.Database.nHibernate.Entities;
using Zooland.Infrastructure.Database.nHibernate.Entities.Extensions;

namespace Zooland.Infrastructure.Queries.Handlers.Animal
{
    internal class SearchAnimalsHandler : IQueryHandler<SearchAnimals, IEnumerable<AnimalDto>>
    {
        private readonly IRepository<AnimalEntity> _repository;

        public SearchAnimalsHandler(IRepository<AnimalEntity> repository)
            => _repository = repository;

        public async Task<IEnumerable<AnimalDto>> HandleAsync(SearchAnimals query)
        {
            var entities = await _repository
                .SearchAsync(d => !query.Classification.HasValue || d.Classification == query.Classification);

            entities = entities.Where(d => string.IsNullOrEmpty(query.Name) || d.Name.ToLower().StartsWith(query.Name.ToLower()));

            return entities.AsDtos();
        }
    }
}