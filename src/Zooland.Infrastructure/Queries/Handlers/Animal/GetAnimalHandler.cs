using System.Threading.Tasks;
using Zooland.Application.DTO;
using Zooland.Application.Queries.Animal;
using Zooland.Application.Queries.Handlers;
using Zooland.Infrastructure.Database;
using Zooland.Infrastructure.Database.nHibernate.Entities;
using Zooland.Infrastructure.Database.nHibernate.Entities.Extensions;

namespace Zooland.Infrastructure.Queries.Handlers.Animal
{
    internal class GetAnimalHandler : IQueryHandler<GetAnimal, AnimalDto>
    {
        private readonly IRepository<AnimalEntity> _repository;

        public GetAnimalHandler(IRepository<AnimalEntity> repository)
            => _repository = repository;

        public async Task<AnimalDto> HandleAsync(GetAnimal query)
        {
            var animnal = await _repository.GetAsync(query.Id);
            return animnal?.AsDto();
        }
    }
}