using System;
using Zooland.Core.Enums;
using Zooland.Core.Exceptions;

namespace Zooland.Core.Entities
{
    public class Place
    {
        public Guid Id { get; }
        public string Name { get; private set; }
        public HouseType House { get; private set; }
        public ZoneType Zone { get; private set; }
        public double MinimalTemperature { get; private set; }

        public Place(Guid id, string name)
        {
            Id = id;
            ChangeName(name);
        }

        public void SetMinimalTemperature(double temperature)
            => MinimalTemperature = temperature;

        public void ChangeName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new DomainException("Name cannot be empty");
            }

            Name = name;
        }

        public void ChangeHouse(HouseType house)
            => House = house;

        public void ChangeZone(ZoneType zone)
            => Zone = zone;
    }
}