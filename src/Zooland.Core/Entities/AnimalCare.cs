using System;
using Zooland.Core.Enums;
using Zooland.Core.Exceptions;

namespace Zooland.Core.Entities
{
    public class AnimalCare
    {
        public Guid Id { get; }
        public Animal Animal { get; }
        public Zookeeper Zookeeper { get; }
        public JobType? Job { get; private set; }
        public string Description { get; private set; }
        public DateTime StartTime { get; private set; }
        public DateTime EndTime { get; private set; }

        public AnimalCare(Guid id, Animal animal, Zookeeper zookeeper)
        {
            Id = id;
            Animal = animal;
            Zookeeper = zookeeper;
            Job = null;
        }

        public void StartJob(JobType job)
        {
            if (EndTime != DateTime.MinValue)
            {
                throw new DomainException("Job already ended");
            }

            if (StartTime != DateTime.MinValue)
            {
                throw new DomainException("Job already started");
            }

            Job = job;
            StartTime = DateTime.Now;
        }

        public void FillDescription(string description)
        {
            Description = description;
        }

        public void EndJob()
        {
            if (Job is null)
            {
                throw new DomainException("There is no job started");
            }

            if (Job == JobType.Other && string.IsNullOrWhiteSpace(Description))
            {
                throw new DomainException("Please fill description");
            }

            EndTime = DateTime.Now;
        }
    }
}