using System;
using Zooland.Core.Enums;
using Zooland.Core.Exceptions;

namespace Zooland.Core.Entities
{
    public class Animal
    {
        public Guid Id { get; }
        public string Category { get; }
        public AnimalClass Classification { get; }
        public string Name { get; private set; }
        public string PhotoUrl { get; private set; }
        public Place PlaceInZoo { get; private set; }

        public Animal(Guid id, string name, string category, AnimalClass classification, string photoUrl)
        {
            Id = id;
            Category = category;
            Classification = classification;

            ChangeName(name);
            ChangePhoto(photoUrl);
        }

        public void ChangeName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new DomainException("Animal name cannot be empty");
            }

            Name = name;
        }

        public void ChangePhoto(string photoUrl)
        {
            if (string.IsNullOrWhiteSpace(photoUrl))
            {
                throw new DomainException("Please provide valid url to animal's picture");
            }

            PhotoUrl = photoUrl;
        }

        public void MoveToOtherPlace(Place placeInZoo)
            => PlaceInZoo = placeInZoo is null ? throw new DomainException("PlaceInZoo can't be null") : placeInZoo;
    }
}