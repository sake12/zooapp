using System;
using Zooland.Core.Exceptions;

namespace Zooland.Core.Entities
{
    public class Zookeeper
    {
        public Guid Id { get; }
        public string Name { get; }
        public string Surname { get; }
        public string PhotoUrl { get; private set; }
        public DateTime Birthdate { get; }
        public Animal FavoriteAnimal { get; private set; }

        public Zookeeper(Guid id, string name, string surname, DateTime birthdate)
        {
            Id = id;
            Name = name;
            Surname = surname;
            Birthdate = birthdate;
        }

        public void ChangeFavoriteAnimal(Animal animal){
            if (animal is null)
            {
                throw new DomainException("Please provide animal from zoo");
            }

            FavoriteAnimal = animal;
        }

        public void ChangePhoto(string photoUrl)
        {
            if (string.IsNullOrWhiteSpace(photoUrl))
            {
                throw new DomainException("Please provide valid url to animal's picture");
            }

            PhotoUrl = photoUrl;
        }

    }
}