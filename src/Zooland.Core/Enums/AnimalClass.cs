namespace Zooland.Core.Enums
{
    public enum AnimalClass
    {
        Bird, Reptile, Amphibian, Mammal, Fish
    }
}