namespace Zooland.Core.Enums
{
    public enum ZoneType
    {
        Wetlands, Tropical, Safari, InTheNest
    }
}