namespace Zooland.Core.Enums
{
    public enum HouseType
    {
        Cage, FreeRun, Aquarium, BehindGlass
    }
}