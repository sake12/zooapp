namespace Zooland.Core.Enums
{
    public enum JobType
    {
        Feed, CleanHouse, GiveMedicine, Play, GiveWater, Other
    }
}