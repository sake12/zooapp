using System;

namespace Zooland.Core.Exceptions
{
    public class DomainException : Exception
    {
        public DomainException(string message)
            : base(message) { }
    }
}