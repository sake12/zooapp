using System;
using System.Threading.Tasks;
using Zooland.Core.Entities;

namespace Zooland.Core.Repositories
{
    public interface IAnimalRepository
    {
        Task<Animal> GetAsync(Guid id);
        Task AddAsync(Animal animal);
        Task UpdateAsync(Animal animal);
        Task DeleteAsync(Animal animal);
    }
}