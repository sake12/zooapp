﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Zooland.Application.Commands.Animals;
using Zooland.Application.Commands.Dispatchers;
using Zooland.Application.DTO;
using Zooland.Application.Queries.Animal;
using Zooland.Application.Queries.Dispatchers;

namespace Zooland.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalsController : ControllerBase
    {
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;

        public AnimalsController(ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher)
        {
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<AnimalDto>>> SearchAnimalsAsync([FromQuery] SearchAnimals query)
        {
            var result = await _queryDispatcher
                .DispatchAsync<SearchAnimals, IEnumerable<AnimalDto>>(query);

            if (result is null || !result.Any())
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AnimalDto>> GetAnimalByIdAsync([FromRoute] GetAnimal query)
        {
            var result = await _queryDispatcher.DispatchAsync<GetAnimal, AnimalDto>(query);

            if (result is null)
            {
                return NotFound(new {message = $"Animal not found in database"});
            }

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAnimalAsync([FromBody] CreateAnimal command)
        {
            await _commandDispatcher.DispatchAsync(command);
            return CreatedAtAction(nameof(GetAnimalByIdAsync), new { id = command.Id }, command);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAnimalAsync([FromRoute] Guid id, [FromBody] UpdateAnimal command)
        {
            command.Bind((animal) => animal.Id, id);
            await _commandDispatcher.DispatchAsync(command);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAnimalAsync([FromRoute] Guid id)
        {
            await _commandDispatcher.DispatchAsync(new DeleteAnimal(id));
            return Ok();
        }
    }
}
