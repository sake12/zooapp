﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Zooland.Application;
using Zooland.Infrastructure;

namespace Zooland.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
            => Configuration = configuration;

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.RegisterApplication();
            services.RegisterInfrastructure();

            services.AddCors(options =>
            {
                options.AddPolicy("ZoolandPolicy",
                    builder =>
                    {
                        builder
                            .AllowAnyOrigin()
                            .AllowAnyHeader()  // !!!   This is not how it should be done   !!!
                            .AllowAnyMethod(); // !!! Making it proper would take much time !!!
                    });
            });

            services.AddMvc().AddJsonOptions(options =>
                {
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                    options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("ZoolandPolicy");
            app.UseInfrastructure();
            app.UseMvc();
        }
    }
}
