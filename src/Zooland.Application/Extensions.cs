using Microsoft.Extensions.DependencyInjection;
using Zooland.Application.Commands;
using Zooland.Application.Events;

namespace Zooland.Application
{
    public static class Extensions
    {
        public static void RegisterApplication(this IServiceCollection services)
        {
            services.AddCommandHandlers();
            services.AddEventHandlers();
        }
    }
}