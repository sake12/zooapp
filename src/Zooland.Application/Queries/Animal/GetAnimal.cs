using System;
using Zooland.Application.DTO;

namespace Zooland.Application.Queries.Animal
{
    public class GetAnimal : IQuery<AnimalDto>
    {
        public Guid Id { get; set; }
    }
}