using System.Collections.Generic;
using Zooland.Application.DTO;
using Zooland.Core.Enums;

namespace Zooland.Application.Queries.Animal
{
    public class SearchAnimals : IQuery<IEnumerable<AnimalDto>>
    {
        public string Name { get; set; }
        public AnimalClass? Classification { get; set; }
    }
}