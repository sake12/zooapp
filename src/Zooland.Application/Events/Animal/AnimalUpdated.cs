using System;

namespace Zooland.Application.Events.Animal
{
    public class AnimalUpdated : IEvent
    {
        public Guid Id { get; }

        public AnimalUpdated(Guid id)
            => Id = id;
    }
}