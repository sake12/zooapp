using System.Threading.Tasks;

namespace Zooland.Application.Events.Animal.Handlers
{
    internal class AnimalCreatedHandler : IEventHandler<AnimalCreated>
    {
        public Task HandleAsync(AnimalCreated @event)
        {
            System.Console.WriteLine($"Animal with id: {@event.Id} has been created");
            return Task.CompletedTask;
        }
    }
}