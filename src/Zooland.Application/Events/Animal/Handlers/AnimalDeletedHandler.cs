using System.Threading.Tasks;

namespace Zooland.Application.Events.Animal.Handlers
{
    internal class AnimalDeletedHandler : IEventHandler<AnimalDeleted>
    {
        public Task HandleAsync(AnimalDeleted @event)
        {
            System.Console.WriteLine($"Animal with id: {@event.Id} has been deleted");
            return Task.CompletedTask;
        }
    }
}