using System.Threading.Tasks;

namespace Zooland.Application.Events.Animal.Handlers
{
    internal class AnimalUpdatedHandler : IEventHandler<AnimalUpdated>
    {
        public Task HandleAsync(AnimalUpdated @event)
        {
            System.Console.WriteLine($"Animal with id: {@event.Id} has been updated");
            return Task.CompletedTask;
        }
    }
}