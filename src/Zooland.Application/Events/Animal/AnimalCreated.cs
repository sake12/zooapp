using System;

namespace Zooland.Application.Events.Animal
{
    public class AnimalCreated : IEvent
    {
        public Guid Id { get; }

        public AnimalCreated(Guid id)
            => Id = id;
    }
}