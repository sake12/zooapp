using System;

namespace Zooland.Application.Events.Animal
{
    public class AnimalDeleted : IEvent
    {
        public Guid Id { get; }

        public AnimalDeleted(Guid id)
            => Id = id;
    }
}