using System.Threading.Tasks;

namespace Zooland.Application.Events.Dispatchers
{
    public interface IEventDispatcher
    {
         Task DispatchAsync<TEvent>(TEvent @event) where TEvent : class, IEvent;
    }
}