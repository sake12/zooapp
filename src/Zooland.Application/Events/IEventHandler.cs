using System.Threading.Tasks;

namespace Zooland.Application.Events
{
    public interface IEventHandler<in TEvent> where TEvent : class, IEvent
    {
         Task HandleAsync(TEvent @event);
    }
}