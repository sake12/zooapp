using System.Threading.Tasks;
using Zooland.Application.Events;

namespace Zooland.Application.Services
{
    public interface IMessageBroker
    {
         Task PublishAsync(params IEvent[] events);
    }
}