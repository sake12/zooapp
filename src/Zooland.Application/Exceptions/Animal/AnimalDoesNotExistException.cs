using System;
using Zooland.Core.Exceptions;

namespace Zooland.Application.Exceptions.Animal
{
    public class AnimalDoesNotExistException : DomainException
    {
        public AnimalDoesNotExistException(Guid id)
            : base($"Animal with id: {id} doesn't exists") { }
    }
}