using System;
using Zooland.Core.Exceptions;

namespace Zooland.Application.Exceptions.Animal
{
    public class AnimalAlreadyExistsException : DomainException
    {
        public AnimalAlreadyExistsException(Guid id)
            : base($"Animal with id: {id} already exists") { }
    }
}