using System.Threading.Tasks;
using Zooland.Application.Events.Animal;
using Zooland.Application.Exceptions.Animal;
using Zooland.Application.Services;
using Zooland.Core.Repositories;

namespace Zooland.Application.Commands.Animals.Handlers
{
    public class UpdateAnimalHandler : ICommandHandler<UpdateAnimal>
    {
        private readonly IAnimalRepository _repository;

        private readonly IMessageBroker _broker;

        public UpdateAnimalHandler(IAnimalRepository repository, IMessageBroker broker)
        {
            _repository = repository;
            _broker = broker;
        }

        public async Task HandleAsync(UpdateAnimal command)
        {
            var animal = await _repository.GetAsync(command.Id);

            if (animal is null)
            {
                throw new AnimalDoesNotExistException(command.Id);
            }

            if (!string.IsNullOrWhiteSpace(command.Name))
            {
                animal.ChangeName(command.Name);
            }

            if (!string.IsNullOrWhiteSpace(command.PhotoUrl))
            {
                animal.ChangePhoto(command.PhotoUrl);
            }

            await _repository.UpdateAsync(animal);
            await _broker.PublishAsync(new AnimalUpdated(command.Id));
        }
    }
}