using System;
using System.Threading.Tasks;
using Zooland.Application.Events.Animal;
using Zooland.Application.Exceptions.Animal;
using Zooland.Application.Services;
using Zooland.Core.Repositories;

namespace Zooland.Application.Commands.Animals.Handlers
{
    public class DeleteAnimalHandler : ICommandHandler<DeleteAnimal>
    {
        private readonly IAnimalRepository _repository;

        private readonly IMessageBroker _broker;

        public DeleteAnimalHandler(IAnimalRepository repository, IMessageBroker broker)
        {
            _repository = repository;
            _broker = broker;
        }

        public async Task HandleAsync(DeleteAnimal command)
        {
            var animal = await _repository.GetAsync(command.Id);

            if (animal is null)
            {
                throw new AnimalDoesNotExistException(command.Id);
            }

            await _repository.DeleteAsync(animal);
            await _broker.PublishAsync(new AnimalDeleted(command.Id));
        }
    }
}