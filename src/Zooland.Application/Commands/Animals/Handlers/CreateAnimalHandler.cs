using System;
using System.Threading.Tasks;
using Zooland.Application.Events.Animal;
using Zooland.Application.Exceptions.Animal;
using Zooland.Application.Services;
using Zooland.Core.Entities;
using Zooland.Core.Repositories;

namespace Zooland.Application.Commands.Animals.Handlers
{
    public class CreateAnimalHandler : ICommandHandler<CreateAnimal>
    {
        private readonly IAnimalRepository _repository;
        private readonly IMessageBroker _broker;

        public CreateAnimalHandler(IAnimalRepository repository, IMessageBroker broker)
        {
            _repository = repository;
            _broker = broker;
        }

        public async Task HandleAsync(CreateAnimal command)
        {
            var animal = await _repository.GetAsync(command.Id);

            if (animal != null)
            {
                throw new AnimalAlreadyExistsException(command.Id);
            }

            var newAnimal = new Animal(command.Id, command.Name, command.Category, command.Classification, command.PhotoUrl);

            await _repository.AddAsync(newAnimal);
            await _broker.PublishAsync(new AnimalCreated(command.Id));
        }
    }
}