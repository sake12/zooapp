using System;
using Zooland.Core.Enums;

namespace Zooland.Application.Commands.Animals
{
    public class CreateAnimal : ICommand
    {
        public Guid Id { get; }
        public string Name { get; }
        public AnimalClass Classification { get; }
        public string Category { get; internal set; }
        public string PhotoUrl { get; internal set; }

        public CreateAnimal(Guid id, string name, string category, AnimalClass classification, string photoUrl)
        {
            Id = id == Guid.Empty ? Guid.NewGuid() : id;
            Name = name;
            Classification = classification;
            PhotoUrl = photoUrl;
            Category = category;
        }
    }
}