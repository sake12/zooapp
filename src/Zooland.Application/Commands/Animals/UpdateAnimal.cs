using System;

namespace Zooland.Application.Commands.Animals
{
    public class UpdateAnimal : ICommand
    {
        public Guid Id { get; }
        public string Name { get; }
        public string PhotoUrl { get; internal set; }

        public UpdateAnimal(Guid id, string name, string photoUrl)
        {
            Id = id;
            Name = name;
            PhotoUrl = photoUrl;
        }
    }
}