using System;
using Zooland.Core.Enums;

namespace Zooland.Application.DTO
{
    public class AnimalDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public AnimalClass Classification { get; set; }
        public string Category { get; set; }
        public string PhotoUrl { get; set; }
    }
}